(function(){
    var init = function() {
        document.querySelector("#generate").addEventListener("click", generate);
    };

    var cloneTemplate = function() {
        var template = document.querySelector('#card-template');
        return document.importNode(template.content, true);
    };

    var generateCard = function(data) {
        var template = cloneTemplate();

        var keys = Object.keys(data);
        for (var i = 0, len = keys.length; i < len; i++) {
            template.querySelector(`[data-value="${keys[i]}"]`).textContent = data[keys[i]];
        }

        var healthContainer = template.querySelector("[data-value='health']");
        healthContainer.innerHTML = "";
        for (var i = 0; i < data.health; i++) {
            var span = document.createElement("span");
            span.classList.add("health-container");
            healthContainer.appendChild(span);
        }

        return template;
    };

    var getRandomValue = function(array) {
        return array[Math.floor(Math.random() * array.length)];
    };

    var generateRandomData = function(amount) {
        var data = [];

        for (var i = 0; i < amount; i++) {
            var ability = getRandomValue(ABILITIES).replace("$NOUN$", getRandomValue(OBJECTS))
            data.push({
                name: getRandomValue(FIRST_NAMES) + " " + getRandomValue(LAST_NAMES),
                health: Math.ceil((Math.random() * 100) % 5),
                ability: ability,
                damage: Math.floor((Math.random() * 100) % 4),
                profession: getRandomValue(PROFESSIONS)
            });
        }

        return data;
    };

    var generate = function() {
        var output = document.querySelector("#output");
        output.innerHTML = "";

        var count = +document.querySelector("#gen-amount-in").value;
        var data = generateRandomData(count);

        for (var i = 0, len = data.length; i < len; i++) {
            var card = generateCard(data[i]);
            output.appendChild(card);
        }
    };

    init();
}());